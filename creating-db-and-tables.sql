create database `journal_task` ;
use `journal_task`;

create table `author` (
	id int primary key auto_increment not null,
    first_name  varchar(80) not null, 
    last_name varchar(120) not null, 
    created_at datetime not null default now(),
    updated_at datetime default null
);

create table `journal` (
	id int primary key auto_increment not null,
    name varchar(80) not null,
	created_at datetime not null default now(),
	updated_at datetime default null
);

create table `journalEntry` (
	id int primary key auto_increment not null,
    title varchar(130) not null,
    `text` text,
	created_at datetime not null default now(),
	updated_at datetime default null,
    author_id int not null,
    journal_id int not null,
    foreign key (author_id) references author(id),
    foreign key (journal_id) references journal(id)
);

create table `tag` (
	id int primary key auto_increment not null,
	title varchar(120) not null,
	created_at datetime not null default now(),
	updated_at datetime default null
);

create table `journal_tag` (
	id int primary key auto_increment not null,
    tag_id int, 
    journalEntry_id int,
	foreign key (tag_id) references tag(id),
    foreign key (journalEntry_id) references journalEntry(id)
);